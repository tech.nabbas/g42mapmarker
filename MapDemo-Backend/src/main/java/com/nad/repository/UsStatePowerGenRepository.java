package com.nad.repository;

import com.nad.entity.UsStatePowerGenEntity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsStatePowerGenRepository extends JpaRepository<UsStatePowerGenEntity, String> {

    public List<UsStatePowerGenEntity> findByName(String name);
}
