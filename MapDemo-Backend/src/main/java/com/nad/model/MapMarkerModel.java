package com.nad.model;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MapMarkerModel {

    private Position position;
    private Label label;
    private String title;
    private Map<String, String> info;
    private String animation;
}
