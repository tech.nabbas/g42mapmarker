create table us_state_power_gen 
(id varchar(2),
name varchar(100),
lat number,
lng number,
total_gen number,
primary key (id) );