# G42MapMarker



## Getting started

Welcome to G42 Us Power Generation Map 

This project is demo for Us states total power generation

## Technology stack

### Frontend
    Angular 14
    Google Map Angular Api
### Backend
    Java 8
    Maven
    Spring Boot
    h2 DB


## How to Run the project localy
    Backend applicaiton:
        Backend applicaiton is a spring boot applicaiton which can be run as a simple java applicaiton.
    Frontend:
        Use any ide of the choise to clone and build as a angular applicaiton.
        This applicaiton needs Google Map APi key which needs to be added in index.html file (Place holder is added for the same).

## Deployment
    Backend applicaiton is deployed on AWS Elastic Bean stack
        URL: http://mapmarker.eba-abkgc5by.us-east-1.elasticbeanstalk.com/map-service/swagger-ui.html
    Frontend application is deployed on AWS S3 as static website 
        URL: http://g42-us-gen-map.s3-website-us-east-1.amazonaws.com/
