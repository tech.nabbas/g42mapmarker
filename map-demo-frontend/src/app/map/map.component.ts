import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { GoogleMap, GoogleMapsModule, MapInfoWindow, MapMarker } from '@angular/google-maps';
import { MapService } from '../service/map.service';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {CreateNewAutocompleteGroup, SelectedAutocompleteItem, NgAutoCompleteComponent} from "ng-auto-complete";


interface States {
  name: string;
}

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements AfterViewInit, OnInit {


  constructor(private mapService: MapService) {
    this.mapService.getStates().subscribe(response => {
      this.satates = response;
    });
    this.filteredStates = this.stateCtrl.valueChanges.pipe(
      startWith(''),
      map(state => (state ? this._filterStates(state) : this.satates.slice())),
    );
    for(let i=1;i<=53;i++){
      this.numbers.push(i);
    }
  }

  satates: States[] = [];
  stateCtrl = new FormControl('');
  filteredStates!: Observable<States[]>;
  selectedValue!: Number;
  selectedState!: States;
  keyword = 'name';
  numbers:  Array<number> = [];

  @ViewChild('myGoogleMap', { static: false })
  map!: GoogleMap;
  @ViewChild(MapInfoWindow, { static: false })
  info!: MapInfoWindow;
  
  zoom = 8;
  maxZoom = 100;
  minZoom = 2;
  center!: google.maps.LatLngLiteral;
  options: google.maps.MapOptions = {
    zoomControl: true,
    scrollwheel: true,
    disableDoubleClickZoom: false,
    mapTypeId: 'hybrid',
    maxZoom: this.maxZoom,
    minZoom: this.minZoom,
  }
  markers = [] as any;
  infoContent = 'Google Marker Demo'
  totalGen=''
  totalGenPercent=''
  stateInfo=''

  ngOnInit() {
    
  }
  ngAfterViewInit() {
    this.mapService.getMapMarkers().subscribe(response => {
      console.log(response);
      this.zoom=2
      navigator.geolocation.getCurrentPosition((position) => {
        this.center = {
          lat: response[0].position.lat,
          lng: response[0].position.lng,
        }
      })
      for (let item of response) {
        this.markers.push({
          position: {
            lat: item.position.lat,
            lng: item.position.lng,
          },
          label: {
            color: "blue",
            text: item.label.text,
          },
          title: item.title,
          info: item.info,
          options: {
          },
          animation: google.maps.Animation.DROP,
        })
      }
    })
    console.log(this.markers);
  }

  zoomIn() {
    if (this.zoom < this.maxZoom) this.zoom++;
    console.log('Get Zoom', this.map.getZoom());
  }

  zoomOut() {
    if (this.zoom > this.minZoom) this.zoom--;
  }

  eventHandler(event: any, name: string) {
    console.log(event, name);

    // Add marker on double click event
    // if(name === 'mapDblclick'){
    //   this.dropMarker(event)
    // }
  }



  openInfo(marker: MapMarker, content: any) {
    this.totalGen = content["Total_Generation"];
    this.totalGenPercent=content["Tot_Percent"]
    this.stateInfo=content["stateName"];
    this.info.open(marker)
  }

  private _filterStates(value: string): States[] {
    const filterValue = value.toLowerCase();

    return this.satates.filter(state => state.name.toLowerCase().includes(filterValue));
  }


  getTopNValues(){
    this.selectedState={
      "name": ""
    }
    this.mapService.getMapMarkersBySearchCriteria(this.selectedValue).subscribe(response=>{
      this.zoom=2
      this.markers=[];
      navigator.geolocation.getCurrentPosition((position) => {
        this.center = {
          lat: response[0].position.lat,
          lng: response[0].position.lng,
        }
      })
      for (let item of response) {
        this.markers.push({
          position: {
            lat: item.position.lat,
            lng: item.position.lng,
          },
          label: {
            color: "blue",
            text: item.label.text,
          },
          title: item.title,
          info: item.info,
          options: {
          },
          animation: google.maps.Animation.DROP,
        })
      }
    })
  }

  getSelectedState(){
    this.selectedValue=0;
    console.log("selected state : "+this.selectedState.name);
    this.mapService.getMapMarkersByStateName(this.selectedState.name).subscribe(response=>{
      
      console.log(response);
      this.zoom=3
      this.markers=[];
      navigator.geolocation.getCurrentPosition((position) => {
        this.center = {
          lat: response[0].position.lat,
          lng: response[0].position.lng,
        }
      })
      for (let item of response) {
        this.markers.push({
          position: {
            lat: item.position.lat,
            lng: item.position.lng,
          },
          label: {
            color: "blue",
            text: item.label.text,
          },
          title: item.title,
          info: item.info,
          options: {
          },
          animation: google.maps.Animation.DROP,
        })
      }
    })
  }
}
