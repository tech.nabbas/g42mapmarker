import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MapService } from '../service/map.service';

interface States {
  name: string;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {

  satates: States[] = [];
  stateCtrl = new FormControl('');
  filteredStates!: Observable<States[]>;
  selectedValue!: string;
  keyword = 'name';
  numbers:  Array<number> = [];
  constructor(private mapService: MapService) {
    this.mapService.getStates().subscribe(response => {
      this.satates = response;
      console.log(this.satates);
    });
    this.filteredStates = this.stateCtrl.valueChanges.pipe(
      startWith(''),
      map(state => (state ? this._filterStates(state) : this.satates.slice())),
    );
    for(let i=1;i<=53;i++){
      this.numbers.push(i);
    }
   }

  ngOnInit(): void {
  }

  private _filterStates(value: string): States[] {
    const filterValue = value.toLowerCase();

    return this.satates.filter(state => state.name.toLowerCase().includes(filterValue));
  }

  update(event: any){
    console.log(event)
  }

  selectEvent(item: any) {
    console.log(item)
  }

  onChangeSearch(search: string) {
    console.log(search)
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e: any) {
    console.log(e);
  }
}

