import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  constructor(private http: HttpClient) { }

  public getMapMarkers(): Observable<any>{
    return this.http.get(`${environment.baseUrl}/map-service/get-top-n-markers?topN=10`);
  }

  public getMapMarkersBySearchCriteria(topN : any): Observable<any>{
    let params = new HttpParams();
  
    params=  params.append("topN", topN);
    
    return this.http.get<any>(`${environment.baseUrl}/map-service/get-top-n-markers`, {params: params});
  }

  public getMapMarkersByStateName(stateName : any): Observable<any>{
    let params = new HttpParams();
    
    params=  params.append("stateName",stateName);
  
    return this.http.get<any>(`${environment.baseUrl}/map-service/get-by-state-markers`, {params: params});
  }
  public getStates(): Observable<any>{
    return this.http.get(`${environment.baseUrl}/map-service/get-states`);
  }
}
